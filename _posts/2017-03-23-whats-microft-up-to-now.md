---
layout: post
title: "What is Microft up to now?"
description: 
headline: 
modified: 2017-03-23
category: webdevelopment
tags: [jekyll]
imagefeature: 
mathjax: 
chart: 
comments: true
featured: true
---
Actually, and the other thing is, why is IE8 still alive and kicking? But to stop ranting, lets look into what Microsoft is now doing with their latest browser incarnation, Edge(https://www.microsoft.com/en-ph/windows/microsoft-edge).

The browser includes an integrated Adobe Flash Player, a PDF reader and supports asm.js. Ooh web assembly. Nice. So what, we’re now going to be asked to know ASM in the future, and also maybe, Flash thanks to Microsoft. But weight, it’s disabled completely in Chrome. Thank you google. Bring back the old days, when flash was everywhere and everypage was animated using Adobe Flash. How about flash games? Now I freaking get it. Microsoft is for the gamer boy who also happens to be bro-gramming. And somehow that gamer boy still finds IIS to be pretty cool.

Yeah gamer boy, because that’s the most cited reason why you would want to stick with the PC - to be able to play so and so which is released for the PC rather than for the Mac.

If you think about it, with Web Assembly, we might now just have the opportunity to build better performing games on the Browser. Why - because of parralell processing pipelines baby. And not only that, but WebAssembly demos use Unity or Unreal Engine, which now can support compiling to asm.js. A bit awkward to port things that required an entire Operating System behind it, such as Adobe Premier Pro or other video type apps, however it seems the future is Facebook providing a way for their Browser-based virtual reality thing. Now what’s interesting, is games that normally had to be downloaded in the hundreds of megabytes through steam could possibly be streamed through a browser. Heck, the future might mean everything is on the web - meaning hello thin-clients.

Did you know there is also an online version of Visual Studio. So we don’t really need anything more than a browser today, except maybe if you keep some personal files and need to store videos locally. I highly believe that the trend of computer chips getting faster and the networked web getting faster as well as a lesser need for CPU-intensive applications mean that in the future, the specs was would be about how fast your graphics can handle the Web Assembly, not really about how your local CPU processes the information.

I think in the future, we won’t see too many cores per CPU but rather, more powerful GPU’s to equip the next generation of computers supporting Virtual Reality. And it seems that’s what these VR Headsets are doing, using more of the graphics card rather than the CPU.
